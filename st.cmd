require essioc
require wavepro,0.1.3

epicsEnvSet("ENGINEER", "Henrique Silva <henrique.silva@ess.eu>")

epicsEnvSet("IOCNAME", "PBI-FBPM01:FBPM-01:")

epicsEnvSet("SCOPE_IP", "bd-scp02.tn.esss.lu.se")
epicsEnvSet("SCOPE_PREFIX", "PBI-FBCM01:SCP-001:")

# Load general logging modules
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load Wavepro Oscilloscope
iocshLoad("$(wavepro_DIR)/wavepro.iocsh", "PREFIX=$(SCOPE_PREFIX),DEVICE_IP=$(SCOPE_IP)")

# Load FastBPM records
epicsEnvSet("CIRC_BUFFER_SIZE", "1000")
iocshLoad("$(E3_CMD_TOP)/iocsh/fastbpm.iocsh", "P=$(IOCNAME),R=,SCOPE=$(SCOPE_PREFIX),CIRC_BUFFER_SIZE=$(CIRC_BUFFER_SIZE)")

## For commands to be run after iocInit, use the function afterInit()

iocInit()

date
