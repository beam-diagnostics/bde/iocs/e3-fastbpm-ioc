# e3-ioc-fastbpm

ESS FastBPM IOC

## Operation

Used to measure the absolute time-of-flight of the accelerated proton beam between two consecutive Beam Position Monitors.

The time delay is calculated by the Oscilloscope LeCroy Wavepro 404HD and retrieved by the wavepro module in a polling scheme. The value is then converted to beam energy using calc records and loaded into a circular buffer.

More info on hardware and principle of operation available on CHESS [ESS-2853515](https://chess.esss.lu.se/enovia/link/ESS-2853515/21308.51166.37120.15768/valid)
